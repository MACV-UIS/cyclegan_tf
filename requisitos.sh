apt update
apt --yes --force-yes install git
pip install keras==2.2.4
pip install git+https://www.github.com/keras-team/keras-contrib.git
pip install jupyter
pip install matplotlib
pip install progress
pip install Pillow
pip install itk
pip install SimpleITK
pip install nibabel
pip install scikit-image
pip install scikit-learn
pip install opencv-python
pip install --upgrade pip
pip install pandas
pip install tqdm
pip install typing
pip install MedPy
pip install einops
pip install Cython
pip install persim
pip install ripser
apt --yes --force-yes install python3-opencv
