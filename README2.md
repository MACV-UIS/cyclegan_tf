# A deep CT to MRI unpaired translation that preserve ischemic stroke lesions

Gustavo Garzón<sup>1</sup>, Santiago Gomez<sup>1</sup>, Daniel Mantilla<sup>2</sup> and Fabio Martı́nez<sup>1</sup><br>
{gustavo.garzon@saber.uis.edu.co, santiago.gomez1@saber.uis.edu.co, dmantilla528@unab.edu.co, famarcar@saber.uis.edu.co}<br>

<sup>1</sup> Biomedical Imaging, Vision and Learning Laboratory (BIVL2ab). Universidad Industrial de Santader (UIS), Bucaramanga, Colombia.<br>
<sup>2</sup> Clı́nica FOSCAL, Bucaramanga, Colombia.<br><br>

![Pipeline](pipeline.png)

<br>
Stroke is the second-leading cause of death world around. The immediate attention is key to patient prognosis. Ischemic stroke diagnosis typically involves neuroimaging studies (MRI and CT scans) and clinical protocols to characterize lesions and support decisions about treatment to be administered to the patient. Nowadays, multiparametric MRI images are the standard tool to visualize core and penumbra of ischemic stroke, supporting diagnosis and lesion prognosis. Specially, DWI modality (Diffusion Weighted Imaging) allows to quantify the cellular density of the tissue, and therefore allowing to quantify the lesion aggressiveness, and the recognition of micro-circulation properties. Nevertheless, MRI availability at hospitals is not widespread, and acquisition require special conditions requiring considerable time. Contrary, CT scans commonly have major availability but brain structures are poorly delineated, and even worse, ischemic lesions are only visible at advanced stages of the disease. This work introduces a deep generative strategy that allows ischemic stroke lesion translation over synthetic DWI-MRI images. This encoder-decoder architecture, include U-net modules, hierarchically organized, with inter-level connections that preserve brain structures, while codifying an embedding representation. Then a cyclic loss was here implemented to receive CT inputs and decode DWI-MRI images. To avoid mode collapse, this learning is inversely propagated, i.e., from synthetic DWI-MRI images to original CT-scans. Finally, an embedding projection is recovered to show a proper lesion-slice discrimination, regarding control studies.
